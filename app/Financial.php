<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financial extends Model
{
    protected $table = 'financials';

    protected $fillable = [
        'id','project_id','organization_id', 'transaction_datetime','amount','financial_status_id','created_at','updated_at'
    ];
}
