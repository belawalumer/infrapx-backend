<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetClass extends Model
{
    protected $table = 'asset_class';

    protected $fillable = [
        'name'
    ];
}
