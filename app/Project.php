<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $fillable = [
        'asset_id','asset_class_id', 'description','owner_organization_id ','investor_organization_id ','amount','yield','term','date_added','date_issued','date_maturity','project_status_id','latitude','longitutde'
    ];
}
