<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPerformance extends Model
{
    protected $table = 'project_performance';

    protected $fillable = [
        'project_id', 'year', 'performance'
    ];

    public function project(){
        return $this->belongsTo(Project::Class, 'project_id');
    }
}
