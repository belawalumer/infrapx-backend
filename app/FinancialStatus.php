<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialStatus extends Model
{
    protected $table = 'financial_status';

    protected $fillable = [
        'name'
    ];
}
