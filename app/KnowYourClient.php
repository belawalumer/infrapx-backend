<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KnowYourClient extends Model
{
    protected $table = 'know_your_client';

    protected $fillable = [
        'date_completed','file_name','organization_id'
    ];
}
