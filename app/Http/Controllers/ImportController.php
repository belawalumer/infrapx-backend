<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Financial;
use App\ProjectPerformance;
use App\Project;
use App\AssetClass;
use App\FinancialStatus;
use App\KnowYourClient;
use App\Organization;
use App\OrganizationType;
use App\ProjectStatus;
use App\User;
use App\UserRole;
use DateTime;
use App\Imports\FinancialsImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function import(Request $request)
    {
        $asset_class = $financials = $financial_status = $know_your_client = $organization = $organization_type = $project = $project_performance = $project_status = $users = $user_roles = false;
        if($request->has('asset_class'))
        {
            $request_type = 'asset_class';
            $asset_class = true;
        }
        elseif($request->has('financials'))
        {
            $request_type = 'financials';
            $financials = true;
        }
        elseif($request->has('financial_status'))
        {
            $request_type = 'financial_status';
            $financial_status = true;
        }
        elseif($request->has('know_your_client'))
        {
            $request_type = 'know_your_client';
            $know_your_client = true;
        }
        elseif($request->has('organization'))
        {
            $request_type = 'organization';
            $organization = true;
        }
        elseif($request->has('organization_type'))
        {
            $request_type = 'organization_type';
            $organization_type = true;
        }
        elseif($request->has('project'))
        {
            $request_type = 'project';
            $project = true;
        }
        elseif($request->has('project_performance'))
        {
            $request_type = 'project_performance';
            $project_performance = true;
        }
        elseif($request->has('project_status'))
        {
            $request_type = 'project_status';
            $project_status = true;
        }
        elseif($request->has('users'))
        {
            $request_type = 'users';
            $users = true;
        }
        elseif($request->has('user_roles'))
        {
            $request_type = 'user_roles';
            $user_roles = true;
        }
        $this->validateCSV($request,$request_type);
        $path = $request->file($request_type);

        $handle = fopen($path, "r");
        $result = Array();
        fgetcsv($handle); //Removes the first line of headings in the csv
        while($data = fgetcsv($handle)) {
            $result[$data[0]] = $data;
        }
        $datetime = new \DateTime('NOW');

        foreach ($result as $row)
        {
            if ($financials)
                Financial::insert([
                    'project_id' => $row[1],
                    'organization_id' => $row[2],
                    'transaction_datetime' => $row[3],
                    'amount' => $row[4],
                    'financial_status_id' => $row[5],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($financial_status)
                FinancialStatus::insert([
                    'name' => $row[1],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($asset_class)
                AssetClass::insert([
                    'name' => $row[1],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($know_your_client)
                KnowYourClient::insert([
                    'date_completed' => $row[1],
                    'file_name' => $row[2],
                    'organization_id' => $row[3],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($organization)
                Organization::insert([
                    'name' => preg_replace("/\r|\n/", "", $row[1]),
                    'organization_type_id' => $row[2],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($organization_type)
                OrganizationType::insert([
                    'name' => $row[1],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($project)
                Project::insert([
                    'asset_id' => $row[1],
                    'asset_class_id' => $row[2],
                    'description' => $row[3],
                    'owner_organization_id' => $row[4],
                    'investor_organization_id' => $row[5],
                    'amount' => $row[6],
                    'yield' => $row[7],
                    'term' => $row[8],
                    'date_added' => $row[9],
                    'date_issued' => $row[10],
                    'date_maturity' => $row[11],
                    'project_status_id' => $row[12],
                    'latitude' => $row[13],
                    'longitutde' => $row[14],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($project_performance)
                ProjectPerformance::insert([
                    'project_id' => $row[1],
                    'year' => $row[2],
                    'performance' => $row[3],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($project_status)
                ProjectStatus::insert([
                    'name' => $row[1],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($users)
                User::insert([
                    'name' => $row[1].' '.$row[2],
                    'email' => $row[3],
                    'password' => bcrypt($row[4]),
                    'user_role_id' => $row[5],
                    'organization_id' => $row[6],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
            else if ($user_roles)
                UserRole::insert([
                    'name' => $row[1],
                    'created_at'=> $datetime,
                    'updated_at'=> $datetime
                ]);
        }
        
        return response()->json(['message' => $request_type.' '.'CSV file uploaded successfully'], 200);
    }

    function validateCSV($request,$request_type){
        $request->validate([
            // 'csv_file' => 'required|file|mimes:xls,xlsx,csv'
            $request_type => 'required|mimes:csv,txt'
        ]);
    }
}
