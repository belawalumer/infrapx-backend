<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Financial;
use App\ProjectPerformance;
use App\Project;
use App\OrganizationType;
use App\Organization;
use App\AssetClass;
use DB;

class ApiController extends Controller
{   
    public function performanceprojectdata(){
        
        $query = ProjectPerformance::select('year','performance','amount','project_id as bond_id')
        ->join('projects','projects.id','project_performance.project_id')
        ->get();
        return $query;
    }

    public function infraPxDashboardData(){
        $current_year = date("Y");
        $totalYieldAmount = Financial::sum('amount');

        $p_id = ProjectPerformance::where('year', $current_year)->pluck('project_id');
        $annualYieldSpend = Financial::whereIn('project_id',$p_id)->sum('amount');

        $numberOfBonds = Project::select('projects.id as bond_id','amount',DB::raw("TRIM(asset_class.name) as asset_class"),'asset_id',DB::raw("YEAR(date_added) as date_added"),'description as descriptor','investor.name as investor',DB::raw("YEAR(date_issued) as issued"),'latitude','longitutde as longitude',DB::raw("YEAR(date_maturity) as maturity"),'owner.name as owner','project_status.name as status','project_status_id','term','yield')
        ->join('asset_class','asset_class.id','projects.asset_class_id')
        ->join('organization as owner','owner.id','projects.owner_organization_id')
        ->join('organization as investor','investor.id','projects.investor_organization_id')
        ->join('project_status','project_status.id','projects.project_status_id')
        ->get();
        
        $numberOfMunicipalities = OrganizationType::count();
        $numberOfInvestors = Organization::where('organization_type_id',2)->count();
        $bondstotalamount = Project::sum('amount');

        $financialdata = Financial::select('financials.amount','asset_id as asset_class','project_id as bond_id',DB::raw("YEAR(projects.date_added) as date"),'description as descriptor','financial_status.name as status')
        ->join('projects','projects.id','financials.project_id')
        ->join('financial_status','financial_status.id','financials.financial_status_id')
        ->get();

        return response()->json([
            'totalYieldAmount' => number_format($totalYieldAmount),
            'annualYieldSpend' => number_format($annualYieldSpend),
            'numberOfBonds' => $numberOfBonds,
            'numberOfMunicipalities' => number_format($numberOfMunicipalities),
            'numberOfInvestors' => number_format($numberOfInvestors),
            'bondstotalamount' => number_format($bondstotalamount),
            'financialdata' => $financialdata
        ]);
    }

    public function financialsdata(){
        return Financial::join('project_performance','project_performance.project_id','financials.project_id')
        ->select('project_performance.year','financials.amount')
        ->get();
    }
}
