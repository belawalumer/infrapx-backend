<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::get('users', 'AuthController@getalluser'); //getAllUsers
    Route::get('edit-user/{id}', 'AuthController@edituser');
    Route::post('update-user/{id}', 'AuthController@updateuser');
    Route::post('delete-user/{id}', 'AuthController@deleteuser');

    //import
    Route::post('import','ImportController@import');
    Route::get('performance_and_project_data','ApiController@performanceprojectdata'); //landing page
    Route::get('infraPx-dashboard-data','ApiController@infraPxDashboardData');
    Route::get('financialsdata','ApiController@financialsdata');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});